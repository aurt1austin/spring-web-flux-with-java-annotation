package com.example.demo;

import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.example.demo.model.Greeting;
import com.example.demo.task.GreetController;
import com.example.demo.task.GreetingRepo;

import reactor.core.publisher.Mono;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = GreetController.class)
@AutoConfigureDataR2dbc
public class ControllerTest {

	@MockBean
    GreetingRepo repository;
	
	@Autowired
    private WebTestClient webClient;
	
	@Test
    void testCreateEmployee() {
        Greeting greet = new Greeting("test");
        Mockito.when(repository.save(greet)).thenReturn(Mono.just(greet));
 
        webClient.post()
            .uri("/greet/save")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(greet))
            .exchange()
            .expectStatus().isOk();
 
        Mockito.verify(repository, times(1)).save(greet);
    }
	
}
