package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@AutoConfigureDataR2dbc
class AnnotationApplicationTests {

	@Test
	void contextLoads() {
	}

}
