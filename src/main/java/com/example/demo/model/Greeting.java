package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Greeting {

	@Id private Integer id;
	private String message;

	public Greeting(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Greeting{" + "message='" + message + '\'' + '}';
	}

}
