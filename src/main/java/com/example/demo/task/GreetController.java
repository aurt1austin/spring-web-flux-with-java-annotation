package com.example.demo.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Greeting;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("/greet")
@RestController
public class GreetController {

	@Autowired
	private GreetingRepo repo;
	
	@GetMapping("/")
	public Mono<String> init() {
		return Mono.just("init na");
	}
	
	@GetMapping("/all")
	public Flux<Greeting> getAll() {
		return repo.findAll();
	}
	
	@PostMapping("/save")
	public Mono<Greeting> save(@RequestBody Greeting greet) {
		return repo.save(new Greeting(greet.getMessage()));
	}
}
