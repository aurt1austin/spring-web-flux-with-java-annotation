package com.example.demo.task;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.example.demo.model.Greeting;

public interface GreetingRepo extends ReactiveCrudRepository<Greeting, Long> {

}
